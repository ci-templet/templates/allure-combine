# `GitLab CI` Template: `Allure Combine`

## How to use it

```yml
include:
  - project: 'ci-templet/allure-combine'
    ref: 'main'
    file: 'allure-combine.yml'

allure-combine:
  extends: .allure-combine
  variables:
    REPORT_DIRECTORY: './some/path/to/allure/generated/allure-report'
```

### Example
- [GitLab CI Pipeline](https://gitlab.com/ci-templet/examples/allure-combine/-/blob/master/.gitlab-ci.yml)

### Reference:
- [allure-combine cli](https://github.com/MihanEntalpo/allure-single-html-file)
